package com.vince.weatherforecast;

import com.vince.weatherforecast.mailer.MailerMock;
import com.vince.weatherforecast.mailer.domain.Mail;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class WeatherForecastServiceImplTest {

    @Autowired
    private WeatherForecastService weatherForecastService;

    @Autowired
    private MailerMock mailerMock;

    @Test
    public void startForecast() {
        //given
        assertThat(this.mailerMock.getMails(), is(empty()));

        //when
        this.weatherForecastService.startForecast();

        //then
        final Mail expectedMail = constructMail();
        assertThat(this.mailerMock.getMails(), hasSize(1));
        assertThat(this.mailerMock.getMails().get(0).getRecipient(), is(expectedMail.getRecipient()));
        assertThat(this.mailerMock.getMails().get(0).getSubject(), is(expectedMail.getSubject()));
        assertThat(this.mailerMock.getMails().get(0).getMessage(), is(expectedMail.getMessage()));
    }

    private Mail constructMail() {
        return Mail.builder()
                .recipient("test@example.com")
                .message("Het weerbericht van vandaag in Nederland/Breda:<br/><br/>" +
                        "Min. temperatuur : 5<br/>" +
                        "Max. temperatuur : 25<br/>" +
                        "Gemiddelde temperatuur : 17")
                .subject("Het weerbericht van vandaag in Nederland/Breda")
                .build();
    }
}