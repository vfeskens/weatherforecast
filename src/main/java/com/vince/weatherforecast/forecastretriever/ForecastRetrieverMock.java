package com.vince.weatherforecast.forecastretriever;

import com.vince.weatherforecast.forecastretriever.domain.Forecast;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.LocalDate;

@Component
public class ForecastRetrieverMock implements ForecastRetriever {

    private final Clock clock;

    ForecastRetrieverMock(final Clock clock) {
        this.clock = clock;
    }

    @Override
    public Forecast retrieve() {
        return Forecast.builder()
                .country("Nederland")
                .city("Breda")
                .date(LocalDate.now(this.clock))
                .averageTemperature(17)
                .minimumTemperature(5)
                .maximumTemperature(25)
                .build();
    }
}
