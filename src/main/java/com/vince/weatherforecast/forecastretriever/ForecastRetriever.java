package com.vince.weatherforecast.forecastretriever;

import com.vince.weatherforecast.forecastretriever.domain.Forecast;

public interface ForecastRetriever {

    Forecast retrieve();
}
