package com.vince.weatherforecast.forecastretriever.domain;

import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@Builder
public class Forecast {

    private String city;

    private String country;

    private LocalDate date;

    private Integer minimumTemperature;

    private Integer maximumTemperature;

    private Integer averageTemperature;
}
