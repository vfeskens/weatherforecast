package com.vince.weatherforecast;

import com.vince.weatherforecast.forecastretriever.ForecastRetriever;
import com.vince.weatherforecast.forecastretriever.domain.Forecast;
import com.vince.weatherforecast.mailer.Mailer;
import com.vince.weatherforecast.mailer.domain.Mail;
import com.vince.weatherforecast.summarizer.Summarizer;
import com.vince.weatherforecast.summarizer.domain.Summary;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class WeatherForecastServiceImpl implements WeatherForecastService {

    private final ForecastRetriever forecastRetriever;

    private final Summarizer summarizer;

    private final Mailer mailer;

    private final String recipient;

    WeatherForecastServiceImpl(final ForecastRetriever forecastRetriever, final Summarizer summarizer,
                               final Mailer mailer, @Value("${recipient}") final String recipient) {
        this.forecastRetriever = forecastRetriever;
        this.summarizer = summarizer;
        this.mailer = mailer;
        this.recipient = recipient;
    }

    @Scheduled(cron = "0 6 0 * * ?")
    @Override
    public void startForecast() {
        log.info("Starting weather forecast");

        final Forecast forecast = this.forecastRetriever.retrieve();

        final Summary summary = this.summarizer.summarize(forecast);

        this.mailer.send(Mail.builder()
                .recipient(this.recipient)
                .subject(summary.getText().substring(0, summary.getText().indexOf(":<br/>")))
                .message(summary.getText())
                .build());
    }
}
