package com.vince.weatherforecast.mailer;

import com.vince.weatherforecast.mailer.domain.Mail;

public interface Mailer {

    void send(Mail mail);
}
