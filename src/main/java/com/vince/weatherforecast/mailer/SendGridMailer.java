package com.vince.weatherforecast.mailer;

import com.sendgrid.*;
import com.vince.weatherforecast.mailer.domain.Mail;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
@Profile(value = "!test")
@Slf4j
public class SendGridMailer implements Mailer {

    private final String apiKey;

    private final String from;

    SendGridMailer(@Value("${send.grid.api.key}") final String apiKey,
                   @Value("${send.grid.mail.from}") final String from) {
        this.apiKey = apiKey;
        this.from = from;
    }

    @Override
    public void send(final Mail mail) {
        log.info("Sending email to {} with sendgrid.", mail.getRecipient());
        final Email from = new Email(this.from);
        final Email to = new Email(mail.getRecipient());
        final Content content = new Content("text/html", mail.getMessage());
        final com.sendgrid.Mail sendGridMail = new com.sendgrid.Mail(from, mail.getSubject(), to, content);

        final SendGrid sendGrid = new SendGrid(this.apiKey);
        final Request request = new Request();
        try {
            request.setMethod(Method.POST);
            request.setEndpoint("mail/send");
            request.setBody(sendGridMail.build());

            sendGrid.api(request);
        } catch (IOException e) {
            log.error("Exception occurred.", e);
        }
    }
}
