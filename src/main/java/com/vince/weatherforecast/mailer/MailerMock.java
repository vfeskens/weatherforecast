package com.vince.weatherforecast.mailer;

import com.vince.weatherforecast.mailer.domain.Mail;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Profile(value = "test")
@Slf4j
public class MailerMock implements Mailer {

    @Getter
    private final List<Mail> mails;

    MailerMock() {
        this.mails = new ArrayList<>();
    }

    @Override
    public void send(final Mail mail) {
        this.mails.add(mail);
    }
}
