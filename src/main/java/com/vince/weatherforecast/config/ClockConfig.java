package com.vince.weatherforecast.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import java.time.*;

@Configuration
public class ClockConfig {
    @Bean
    @Profile("!test")
    public Clock clock() {
        return Clock.systemDefaultZone();
    }

    @Bean
    @Profile("test")
    public Clock testClock() {
        return Clock.fixed(
                ZonedDateTime.of(
                        LocalDateTime.of(2019, Month.APRIL, 21, 19, 50),
                        ZoneId.of("Europe/Amsterdam"))
                        .toInstant(),
                ZoneId.of("Europe/Amsterdam")
        );
    }
}
