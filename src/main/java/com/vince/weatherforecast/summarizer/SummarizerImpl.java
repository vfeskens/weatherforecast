package com.vince.weatherforecast.summarizer;

import com.vince.weatherforecast.forecastretriever.domain.Forecast;
import com.vince.weatherforecast.summarizer.domain.Summary;
import org.springframework.stereotype.Component;

import java.time.Clock;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
public class SummarizerImpl implements Summarizer {

    private final Clock clock;

    SummarizerImpl(final Clock clock) {
        this.clock = clock;
    }

    @Override
    public Summary summarize(final Forecast forecast) {
        final String text =
                String.format("Het weerbericht van %s in %s/%s:<br/><br/>" +
                                "Min. temperatuur : %s<br/>" +
                                "Max. temperatuur : %s<br/>" +
                                "Gemiddelde temperatuur : %s",
                        determineDayString(forecast.getDate()),
                        forecast.getCountry(),
                        forecast.getCity(),
                        forecast.getMinimumTemperature(),
                        forecast.getMaximumTemperature(),
                        forecast.getAverageTemperature()
                );

        return Summary.builder()
                .text(text)
                .build();
    }

    private String determineDayString(final LocalDate localDate) {
        if (localDate.equals(LocalDate.now(this.clock))) {
            return "vandaag";
        } else {
            return DateTimeFormatter.ofPattern("dd-MM-yyyy")
                    .format(localDate);
        }
    }
}
