package com.vince.weatherforecast.summarizer.domain;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Summary {

    private String text;
}
