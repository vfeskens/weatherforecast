package com.vince.weatherforecast.summarizer;


import com.vince.weatherforecast.forecastretriever.domain.Forecast;
import com.vince.weatherforecast.summarizer.domain.Summary;

public interface Summarizer {

    Summary summarize(Forecast forecast);
}
