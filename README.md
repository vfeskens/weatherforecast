# Weather Forecast (work in progress)

This application will retrieve the weather forecast at the beginning the day and will send an email to inform.

Inspired by the ports and adapter [kata](https://github.com/swkBerlin/ports-and-adapters)

## Technologies used:
- Java 8
- Spring boot
- Lombok
- Docker maven plugin

## TODO:
- Add a forecast retriever
- Add tests for ports and adapters
- Adjust summarizer logic
- Extend forecast domain class